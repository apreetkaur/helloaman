//
//  CountryViewController.swift
//  HelloAman
//
//  Created by neeraj bala on 23/12/21.
//

import UIKit

class CountryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
   
    @IBOutlet weak var country: UITableView!
    var array : [String] = ["USA", "China","Australia","Canada","England"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        country.register(UINib(nibName: "RowCellTableViewCell", bundle: nil),forCellReuseIdentifier: "RowCellTableViewCell")
        country.delegate = self
        country.dataSource = self
        print("array count \(array.count)")
        
        
      // Do any additional setup after loading the view.
    }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return array.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RowCellTableViewCell", for: indexPath) as! RowCellTableViewCell
            cell.image1.image = UIImage(named: array[indexPath.row])
            cell.label1.text = array[indexPath.row]
            return cell
        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstViewController") as? FirstViewController
            vc?.labelText = "USA"
            vc?.image12 = "USA"
            self.navigationController?.pushViewController(vc!, animated: true)
        }else if indexPath.row == 1{
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstViewController") as? FirstViewController
            vc?.labelText = "China"
            vc?.image12 = "China"
            self.navigationController?.pushViewController(vc!, animated: true)
        }
            if indexPath.row == 2{
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstViewController") as? FirstViewController
                vc?.labelText = "Australia"
                vc?.image12 = "Australia"
                self.navigationController?.pushViewController(vc!, animated: true)
            }
               else if indexPath.row == 3{
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstViewController") as? FirstViewController
                    vc?.labelText = "Canada"
                    vc?.image12 = "Canada"
                    self.navigationController?.pushViewController(vc!, animated: true)
               }
         if indexPath.row == 4{
             let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstViewController") as? FirstViewController
             vc?.labelText = "England"
             vc?.image12 = "England"
             self.navigationController?.pushViewController(vc!, animated: true)
     }
   }
}
