//
//  SecondViewController.swift
//  HelloAman
//
//  Created by neeraj bala on 22/12/21.
//

import UIKit





class SecondViewController: UIViewController {
    @IBOutlet weak var imageLabel: UILabel!
    
    @IBOutlet weak var imageFlower: UIImageView!
    
    var labelText: String = ""
    var image : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageLabel.text = labelText
        self.imageFlower.image = UIImage(named: image)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
