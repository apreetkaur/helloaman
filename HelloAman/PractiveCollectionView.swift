//
//  PractiveCollectionView.swift
//  HelloAman
//
//  Created by neeraj bala on 23/12/21.
//

import UIKit

class PractiveCollectionView: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var colectionView: UICollectionView!
    
    let aray : [String] = ["Domino","Pizza Hut","Pizza Max","Pizza Point","California"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.colectionView.register(UINib(nibName: "PracticeCollectionCell", bundle: nil), forCellWithReuseIdentifier: "PracticeCollectionCell")
        // Do any additional setup after loading the view.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PracticeCollectionCell", for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let collectionViewWidth = self.view.bounds.width
            
            return CGSize(width: collectionViewWidth/4 , height: collectionViewWidth/4)
            
        }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
